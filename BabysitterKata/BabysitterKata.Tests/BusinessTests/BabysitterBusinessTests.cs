﻿using System;
using BabysitterKata.Business;
using BabysitterKata.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BabysitterKata.Tests.BusinessTests
{
    [TestClass]
    public class BabysitterBusinessTests
    {
        private static readonly TimeManagement Management = new TimeManagement();
        private static readonly RateManagement RateManagement = new RateManagement();
        private readonly BabysitterBusiness _business = new BabysitterBusiness(Management, RateManagement);

        #region RecordStartTime
        [TestMethod]
        public void BabysitterBusiness_BabysitterEntersValidTime_ReturnsSuccessMessage()
        {
            // Arrange
            var dateTime = new DateTime(2016, 02, 27, 18, 0, 0);

            // Act
            var result = _business.RecordStartTime(dateTime);

            // Assert
            Assert.AreEqual(result, "    Start time recorded");
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void BabysitterBusiness_BabysitterStartsBeforeFive_ReturnsFormatExceptionError()
        {
            // Arrange
            var dateTime = new DateTime(2016, 02, 27, 16, 0, 0);

            // Act
            var result = _business.RecordStartTime(dateTime);

            // Assert
            Assert.AreEqual(result, "Invalid Entry");
        }
        #endregion

        #region RecordEndTime
        [TestMethod]
        public void BabsitterBusiness_BabysitterEntersValidTime_ReturnsSuccessMessage()
        {
            // Arrange
            var dateTime = new DateTime(2016, 02, 27, 20, 0, 0);

            // Act
            var result = _business.RecordEndTime(dateTime);

            // Assert
            Assert.AreEqual(result, "    End time recorded");
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void BabsitterBusiness_BabysitterStaysAfterFour_ReturnsFormatException()
        {
            // Arrange
            var dateTime = new DateTime(2016, 02, 27, 5, 0, 0);

            // Act
            var result = _business.RecordEndTime(dateTime);

            // Assert
            Assert.AreEqual(result, "Invalid Entry");
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void BabsitterBusiness_BabysitterEndsBeforeStarting_ReturnsInvalidOperationException()
        {
            // Arrange
            var dateTime = new DateTime(2016, 02, 27, 5, 0, 0);

            // Act
            var result = _business.RecordEndTime(dateTime);

            // Assert
            Assert.AreEqual(result, "Invalid Entry");
        }
        #endregion

        #region StringParser
        [TestMethod]
        public void BabysitterBusiness_ParseString_ReturnsAmOrPmTime()
        {
            // Arrange
            var dateTime = new DateTime(2016, 02, 27, 20, 0, 0);

            // Act
            var result = _business.StringParser(dateTime);

            // Assert
            Assert.AreEqual(result, "PM");
        }
        #endregion

        #region CalculatePaymentDue()
        [TestMethod]
        public void BabysitterBusiness_AllTimeStampsAreRecorded_ReturnsCorrectAmountDue()
        {
            // Arrange
            var totalHours = new RateManagement
            {
                TotalHoursBeforeBedtime = TimeSpan.FromHours(2),
                TotalBedtimeHours = TimeSpan.FromHours(3),
                TotalOvertimeHours = TimeSpan.FromHours(4)
            };

            var timeManagement = new TimeManagement();

            // Act
            var business = new BabysitterBusiness(timeManagement, totalHours);
            var result = business.CalculatePaymentDue();

            // Assert
            Assert.AreEqual(result, 112M);
        }

        [TestMethod]
        public void BabysitterBusiness_NoBedtimeRecorded_ReturnsCorrectAmountDue()
        {
            // Arrange
            var totalHours = new RateManagement
            {
                TotalHoursBeforeBedtime = TimeSpan.FromHours(2),
                TotalBedtimeHours = TimeSpan.FromHours(0),
                TotalOvertimeHours = TimeSpan.FromHours(4)
            };

            var timeManagement = new TimeManagement();

            // Act
            var business = new BabysitterBusiness(timeManagement, totalHours);
            var result = business.CalculatePaymentDue();

            // Assert
            Assert.AreEqual(result, 88M);
        }

        [TestMethod]
        public void BabysitterBusiness_AllTimeStampsAreRecordedWithAdditionalMinutes_ReturnsCorrectAmountDue()
        {
            // Arrange
            var totalHours = new RateManagement
            {
                TotalHoursBeforeBedtime = TimeSpan.FromHours(2.5),
                TotalBedtimeHours = TimeSpan.FromHours(3.5),
                TotalOvertimeHours = TimeSpan.FromHours(4.5)
            };

            var timeManagement = new TimeManagement();

            // Act
            var business = new BabysitterBusiness(timeManagement, totalHours);
            var result = business.CalculatePaymentDue();

            // Assert
            Assert.AreEqual(result, 148M);
        }
        #endregion

        #region CalculateTotalHours
        [TestMethod]
        public void BabysitterBusiness_BabysitterRecordsAllTimeStamps_UpdatesAllRateManagementModelProperties()
        {
            // Arrange
            var rateManagement = new RateManagement
            {
                TotalHoursBeforeBedtime = TimeSpan.FromHours(0),
                TotalBedtimeHours = TimeSpan.FromHours(0),
                TotalOvertimeHours = TimeSpan.FromHours(0)
            };

            var timeManagement = new TimeManagement
            {
                StartTime = DateTime.Today.AddHours(17),
                BedTime = DateTime.Today.AddHours(20),
                EndTime = DateTime.Today.AddHours(1),
                ChildHasBedTime = true
            };

            // Act
            var business = new BabysitterBusiness(timeManagement, rateManagement);
            var result = business.CalculateTotalHours();
            business.CalculatePaymentDue();

            // Assert
            Assert.AreEqual(result.TotalHoursBeforeBedtime, TimeSpan.FromHours(3));
            Assert.AreEqual(result.TotalBedtimeHours, TimeSpan.FromHours(0));
            Assert.AreEqual(result.TotalOvertimeHours, TimeSpan.FromHours(1));
        }

        [TestMethod]
        public void BabysitterBusiness_BabysitterRecordsStartAndEndTimeStamps_UpdatesAllRateManagementModelProperties()
        {
            // Arrange
            var rateManagement = new RateManagement
            {
                TotalHoursBeforeBedtime = TimeSpan.FromHours(0),
                TotalBedtimeHours = TimeSpan.FromHours(0),
                TotalOvertimeHours = TimeSpan.FromHours(0)
            };

            var timeManagement = new TimeManagement
            {
                StartTime = DateTime.Today.AddHours(17),
                BedTime = DateTime.Today.AddHours(20),
                EndTime = DateTime.Today.AddHours(1),
                ChildHasBedTime = false
            };

            // Act
            var business = new BabysitterBusiness(timeManagement, rateManagement);
            var result = business.CalculateTotalHours();
            business.CalculatePaymentDue();

            // Assert
            Assert.AreEqual(result.TotalHoursBeforeBedtime, TimeSpan.FromHours(7));
            Assert.AreEqual(result.TotalBedtimeHours, TimeSpan.FromHours(0));
            Assert.AreEqual(result.TotalOvertimeHours, TimeSpan.FromHours(1));
        }
        #endregion
    }
}
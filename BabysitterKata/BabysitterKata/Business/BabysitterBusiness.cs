﻿using System;
using System.Globalization;
using BabysitterKata.Models;

namespace BabysitterKata.Business
{
    public class BabysitterBusiness
    {
        private readonly TimeManagement _timeManagement;
        private readonly RateManagement _rateManagement;
        private readonly HourlyRate _hourlyRate = new HourlyRate
        {
            StartingRate = 12M,
            BedtimeRate = 8M,
            OvertimeRate = 16M
        };

        public BabysitterBusiness(TimeManagement timeManagement, RateManagement rateManagement)
        {
            _timeManagement = timeManagement;
            _rateManagement = rateManagement;
        }

        public DateTime GetUserInput()
        {
            var currentTime = DateTime.Parse(Console.ReadLine());
            return currentTime;
        }

        public string RecordStartTime(DateTime startTime)
        {
            // Validate that babysitter does not start before 5pm
            if (startTime.TimeOfDay < TimeSpan.FromHours(17))
            {
                throw new FormatException();
            }

            _timeManagement.StartTime = startTime;
            return "    Start time recorded";
        }

        public string RecordEndTime(DateTime endTime)
        {
            // Validate that babysitter does not stay after 4am
            if ((StringParser(endTime) == "AM") && (endTime.TimeOfDay > TimeSpan.FromHours(4)))
            {
                throw new FormatException();
            }

            // Validate taht babysitter does not end before they start
            if ((StringParser(endTime) == "PM") && (endTime.TimeOfDay < _timeManagement.StartTime.TimeOfDay))
            {
                throw new InvalidOperationException();
            }

            _timeManagement.EndTime = endTime;
            return "    End time recorded";
        }

        public string StringParser(DateTime time)
        {
            return time.ToString("tt", CultureInfo.InvariantCulture);
        }

        public string RecordBedTime(string childHasBedTime)
        {
            if (childHasBedTime.ToLower() == "yes")
            {
                _timeManagement.ChildHasBedTime = true;
                // Get bedtime from sitter
                Console.Write("    Enter Bedtime: ");
                var bedTime = GetUserInput();

                // store in model
                _timeManagement.BedTime = bedTime;

                // verify that kid can go to bed after midnight and that kid is not put to bed before starting
                if ((StringParser(bedTime) == "PM") && (bedTime < _timeManagement.StartTime))
                {
                    throw new SystemException();
                }

                // calculate bedtime hours
                var midnight = TimeSpan.FromHours(12);
                var totalBedtimeHours = midnight + DateTime.Parse(midnight.ToString()).Subtract(DateTime.Parse(bedTime.TimeOfDay.ToString()));

                _rateManagement.TotalBedtimeHours = totalBedtimeHours;
                return "    Bedtime recorded";
            }

            return "    No bedtime to record";
        }

        public RateManagement CalculateTotalHours()
        {
            var totalHoursInDay = TimeSpan.FromHours(24);
            var endTimeParser = StringParser(_timeManagement.EndTime);

            TimeSpan duration;
            if (endTimeParser == "AM")
            {
                // store overtime hours
                var totalOverTimeHours = _timeManagement.EndTime.TimeOfDay;
                _rateManagement.TotalOvertimeHours = totalOverTimeHours;

                // calculate left over time before midnight
                duration = totalHoursInDay + DateTime.Parse(_timeManagement.EndTime.TimeOfDay.ToString()).Subtract(DateTime.Parse(_timeManagement.StartTime.TimeOfDay.ToString()));

                // checking if kid has bedtime
                var rateBeforeBedtime = duration - totalOverTimeHours;
                if (_timeManagement.ChildHasBedTime)
                {
                    rateBeforeBedtime = DateTime.Parse(_timeManagement.BedTime.TimeOfDay.ToString()).Subtract(DateTime.Parse(_timeManagement.StartTime.TimeOfDay.ToString()));
                }

                _rateManagement.TotalHoursBeforeBedtime = rateBeforeBedtime;

                return _rateManagement;
            }

            duration = DateTime.Parse(_timeManagement.EndTime.TimeOfDay.ToString()).Subtract(DateTime.Parse(_timeManagement.StartTime.TimeOfDay.ToString()));

            _rateManagement.TotalHoursBeforeBedtime = duration;
            return _rateManagement;
        }

        public decimal CalculatePaymentDue()
        {
            // calculate amount due for total hours
            var beforeBedtimeAmountDue = _hourlyRate.StartingRate * _rateManagement.TotalHoursBeforeBedtime.Hours;
            var duringBedtimeAmountDue = _hourlyRate.BedtimeRate * _rateManagement.TotalBedtimeHours.Hours;
            var afterHoursAmountDue = _hourlyRate.OvertimeRate * _rateManagement.TotalOvertimeHours.Hours;

            // rounding up if not at start of hour
            if (_rateManagement.TotalHoursBeforeBedtime.Minutes != 00)
            {
                beforeBedtimeAmountDue += _hourlyRate.StartingRate;
            }

            if (_rateManagement.TotalBedtimeHours.Minutes != 00)
            {
                duringBedtimeAmountDue += _hourlyRate.BedtimeRate;
            }

            if (_rateManagement.TotalOvertimeHours.Minutes != 00)
            {
                afterHoursAmountDue += _hourlyRate.OvertimeRate;
            }

            return beforeBedtimeAmountDue + duringBedtimeAmountDue + afterHoursAmountDue;
        }
    }
}
﻿using System;

namespace BabysitterKata.Models
{
    public class RateManagement 
    {
        public TimeSpan TotalBedtimeHours { get; set; }
        public TimeSpan TotalOvertimeHours { get; set; }
        public TimeSpan TotalHoursBeforeBedtime { get; set; }
    }

    public interface IRateManagement
    {
        TimeSpan TotalBedtimeHours { get; set; }
        TimeSpan TotalOvertimeHours { get; set; }
        TimeSpan TotalHoursBeforeBedtime { get; set; }
    }
}

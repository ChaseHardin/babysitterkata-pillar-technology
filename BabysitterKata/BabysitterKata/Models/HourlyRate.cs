﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BabysitterKata.Models
{
    public class HourlyRate
    {
        public decimal StartingRate { get; set; }
        public decimal BedtimeRate { get; set; }
        public decimal OvertimeRate { get; set; }
    }
}

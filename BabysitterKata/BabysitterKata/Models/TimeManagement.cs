﻿using System;

namespace BabysitterKata.Models
{
    public class TimeManagement
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool ChildHasBedTime { get; set; }
        public DateTime BedTime { get; set; }
    }
}

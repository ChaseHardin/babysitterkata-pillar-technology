﻿using System;
using BabysitterKata.Business;
using BabysitterKata.Models;

namespace BabysitterKata
{
    public class Program
    {
        static void Main(string[] args)
        {
            var management = new TimeManagement();
            var rateManagement = new RateManagement();
            var business = new BabysitterBusiness(management, rateManagement);

            try
            {
                // Enter start time
                Console.Write("Enter Start Time: ");
                Console.WriteLine(business.RecordStartTime(business.GetUserInput()));

                // Enter bed time
                Console.WriteLine();
                Console.Write("Did child go to bed? ('Yes/No') ");
                business.RecordBedTime(Console.ReadLine());

                // Enter end time
                Console.WriteLine();
                Console.Write("Enter End Time: ");
                Console.WriteLine(business.RecordEndTime(business.GetUserInput()));

                // Calculate hours
                business.CalculateTotalHours();

                // Calculated amount due
                Console.WriteLine();
                Console.Write("Amount Due: ");
                Console.WriteLine("$" + business.CalculatePaymentDue());
            }
            catch (FormatException)
            {
                Console.WriteLine("Invalid Entry\n");
            }
            catch (Exception)
            {
                Console.WriteLine("Error occured.");
            }
        }
    }
}
